#! /usr/bin/env python

"""
Send MIDI over to Aeolus and get WAV file out, everything in freewheel mode
(non-realtime).
"""

from mido import MidiFile, open_output, Message
from jack import Client

import sys

from aeolus_config import AEOLUS_DEFINITIONS

MIDI_IN_NAME = "midi_in"
CLIENT_NAME = "pyclient"


def start_aeolus():
    """
    Starts aeolus and returns (input_MIDI_port_name, output_audio_port_name).
    """
    # TODO really start it :)

    return ("aeolus:In 130:0", "aeolus:out")


def start_jack_client():
    client = Client(CLIENT_NAME)
    client.activate()
    return client


def connect_ports(client, src, dest):
    """
    Connects if the connection does not exist, otherwise disconnects and
    reconnects
    """
    try:
        client.disconnect(src, dest)
    except BaseException as e:
        print(f"Setting new connection '{src}' to '{dest}'...")
    client.connect(src, dest)


def connect_aeolus(client, aeolus_out_name):
    """
    Connect aeolus output to system playback.
    """
    connect_ports(client, f"{aeolus_out_name}.L", "system:playback_1")
    connect_ports(client, f"{aeolus_out_name}.R", "system:playback_2")


class AeolusStop:
    def __init__(self, group_name, stop_name, definitions=AEOLUS_DEFINITIONS):
        self.definitions = definitions
        self.group_name = group_name
        self.group_id = None
        self.group_index = None
        self.stop_name = stop_name
        self.stop_index = None
        self.stop_id = None

        try:
            (self.group_index, self.group_id) = [
                (ix, gp["group_id"])
                for (ix, gp) in enumerate(self.definitions)
                if gp["group_name"] == self.group_name
            ][0]
        except IndexError:
            raise ValueError(f"Unknown group '{self.group_name}', not defined.")

        try:
            group_object = self.definitions[self.group_index]
            (self.stop_index, self.stop_id) = [
                (ix, st["stop_id"])
                for (ix, st) in enumerate(group_object["stops"])
                if st["name"] == self.stop_name
            ][0]
        except IndexError:
            raise ValueError(
                f"Unknown stop '{self.stop_name}', not in group '{self.group_name}'."
            )


def activate_stop(stop, aeolus_in_port_name):
    """"""
    msg_1 = Message('control_change', channel=0, control=98, value=stop.group_id)
    msg_2 = Message('control_change', channel=0, control=98, value=stop.stop_id)
    with open_output(aeolus_in_port_name) as outport:
        outport.send(msg_1)
        outport.send(msg_2)
    return


if __name__ == "__main__":
    client = start_jack_client()
    aeolus_in, aeolus_out = start_aeolus()
    connect_aeolus(client, aeolus_out)

    # FIXME
    stop_spec_list = [
        ('I', 'Principal (8)'),
        ('III', 'Quintadena (8)'),
        ('I', 'I+III')
    ]

    for group_name, stop_name in stop_spec_list:
        stop = AeolusStop(group_name, stop_name)
        activate_stop(stop, aeolus_in)

    midfile = MidiFile("SonataIV-1.midi")
    with open_output(aeolus_in) as outport:
        try:
            for msg in midfile.play():
                outport.send(msg)
        except BaseException as e:
            print(e)
            outport.reset()
    print("Exiting")
