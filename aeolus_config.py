#!/usr/bin/env python

# library of Aeolus keyboards and associated stops
AEOLUS_DEFINITIONS = [
    {
        "group_name": "III",
        "group_id": 96,
        "stops": [
            {
                "name": "Principal (8)",
                "stop_id": 0,
            },
            {
                "name": "Gemshorn (8)",
                "stop_id": 1,
            },
            {
                "name": "Quintadena (8)",
                "stop_id": 2,
            },
            {
                "name": "Suabile (8)",
                "stop_id": 3,
            },
            {
                "name": "Rohrflöte (4)",
                "stop_id": 4,
            },
            {
                "name": "Dulzflöte (4)",
                "stop_id": 5,
            },
            {
                "name": "Quintflöte (2 2/3)",
                "stop_id": 6,
            },
            {
                "name": "Superoctave (2)",
                "stop_id": 7,
            },
            {
                "name": "Sifflet (1)",
                "stop_id": 8,
            },
            {
                "name": "Cymbel VI",
                "stop_id": 9,
            },
            {
                "name": "Oboe",
                "stop_id": 10,
            },
            {
                "name": "Tremulant",
                "stop_id": 11,
            },
        ],
    },
    {
        "group_name": "II",
        "group_id": 97,
        "stops": [
            {
                "name": "Rohrflöte (8)",
                "stop_id": 0,
            },
            {
                "name": "Harmonic Flute (8)",
                "stop_id": 1,
            },
            {
                "name": "Flaute Dolce (4)",
                "stop_id": 2,
            },
            {
                "name": "Nasard (2 2/3)",
                "stop_id": 3,
            },
            {
                "name": "Ottavina (2)",
                "stop_id": 4,
            },
            {
                "name": "Tertia (1 3/5)",
                "stop_id": 5,
            },
            {
                "name": "Sesquialtera",
                "stop_id": 6,
            },
            {
                "name": "Septime",
                "stop_id": 7,
            },
            {
                "name": "None",
                "stop_id": 8,
            },
            {
                "name": "Krumhorn",
                "stop_id": 9,
            },
            {
                "name": "Melodia",
                "stop_id": 10,
            },
            {
                "name": "Tremulant",
                "stop_id": 11,
            },
            {
                "name": "II+III",
                "stop_id": 12,
            },
        ],
    },
    {
        "group_name": "I",
        "group_id": 98,
        "stops": [
            {
                "name": "Principal (8)",
                "stop_id": 0,
            },
            {
                "name": "Principal (4)",
                "stop_id": 1,
            },
            {
                "name": "Octave (2)",
                "stop_id": 2,
            },
            {
                "name": "Octave (1)",
                "stop_id": 3,
            },
            {
                "name": "Quint (5 1/3)",
                "stop_id": 4,
            },
            {
                "name": "Quint (2 2/3)",
                "stop_id": 5,
            },
            {
                "name": "Tibia (8)",
                "stop_id": 6,
            },
            {
                "name": "Celesta (8)",
                "stop_id": 7,
            },
            {
                "name": "Flöte (8)",
                "stop_id": 8,
            },
            {
                "name": "Flöte (4)",
                "stop_id": 9,
            },
            {
                "name": "Flöte (2)",
                "stop_id": 10,
            },
            {
                "name": "Cymbel VI",
                "stop_id": 11,
            },
            {
                "name": "Mixtur",
                "stop_id": 12,
            },
            {
                "name": "Trumpet",
                "stop_id": 13,
            },
            {
                "name": "I+II",
                "stop_id": 14,
            },
            {
                "name": "I+III",
                "stop_id": 15,
            },
        ],
    },
    {
        "group_name": "P",
        "group_id": 99,
        "stops": [
            {
                "name": "Subbass (16)",
                "stop_id": 0,
            },
            {
                "name": "Principal (16)",
                "stop_id": 1,
            },
            {
                "name": "Principal (8)",
                "stop_id": 2,
            },
            {
                "name": "Principal (4)",
                "stop_id": 3,
            },
            {
                "name": "Octave (2)",
                "stop_id": 4,
            },
            {
                "name": "Octave (1)",
                "stop_id": 5,
            },
            {
                "name": "Quint (5 1/3)",
                "stop_id": 6,
            },
            {
                "name": "Quint (2 2/3)",
                "stop_id": 7,
            },
            {
                "name": "Mixtur",
                "stop_id": 8,
            },
            {
                "name": "Fagott (16)",
                "stop_id": 9,
            },
            {
                "name": "Trombone (16)",
                "stop_id": 10,
            },
            {
                "name": "Bombarde (32)",
                "stop_id": 11,
            },
            {
                "name": "Trumpet",
                "stop_id": 12,
            },
            {
                "name": "P+I",
                "stop_id": 13,
            },
            {
                "name": "P+II",
                "stop_id": 14,
            },
            {
                "name": "P+III",
                "stop_id": 15,
            },
        ],
    },
]
